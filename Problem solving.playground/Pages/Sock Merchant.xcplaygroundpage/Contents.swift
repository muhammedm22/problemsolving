import Foundation

func sockMerchant(n: Int, ar: [Int]) -> Int {
    // Write your code here
    var checkSet: Set<Int> = []
    var count = 0
    for sock in ar {
        if !checkSet.contains(sock) {
             checkSet.insert(sock)
        } else {
             count += 1
            checkSet.remove(sock)
        }
    }
    return count
}

let array = [1, 2, 2, 3, 3, 1, 9, 0, 10]


print(sockMerchant(n: array.count, ar: array))
